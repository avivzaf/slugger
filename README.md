# avivz_rapyd/Slugger

[![marker](https://badgen.net/npm/v/@avivz_rapyd/slugger)](https://www.npmjs.com/package/@avivz_rapyd/slugger)
[![jest](https://jestjs.io/img/jest-badge.svg)](https://github.com/facebook/jest)
[![jest](https://img.shields.io/bundlephobia/min/@avivz_rapyd/slugger.svg)](https://www.npmjs.com/package/@avivz_rapyd/slugger)

A simple function that divide words with '-'

## installation
```
npm i -D @avivz_rapyd/slugger
```

## CommonJS
```javascript
const marker = require('@avivz_rapyd/slugger'); 
```

## ES6 modules
```javascript
import marker from '@avivz_rapyd/slugger'; 
```

## Usage
```javascript
slugger("hello world","my name","is "); // output : hello-world-my-name-is
```