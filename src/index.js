export function slugger(...strParams){
    return strParams.map(str => str.split(" ").join("-")).join("-");
}